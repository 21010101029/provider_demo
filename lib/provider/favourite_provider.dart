

import 'package:flutter/material.dart';

class FavouriteProvider with ChangeNotifier{
  final List _list=[];
  List get  list => _list ;
  void addValues(int index){
    if(_list.contains(index)){
      _list.remove(index);
    }else{  
      _list.add(index);
    }
    notifyListeners();
  }
}