import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/count_provider.dart';
import 'package:provider_overview_01/provider/exampleone_provider.dart';
import 'package:provider_overview_01/provider/favourite_provider.dart';
import 'package:provider_overview_01/screens/example_one.dart';
import 'package:provider_overview_01/screens/favourite.dart';
import 'package:provider_overview_01/utils/provider_utils.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => CountProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ExampleOneProvider(),
        ),
        ChangeNotifierProvider(create: (context) => FavouriteProvider(),),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          appBarTheme: const AppBarTheme(
              backgroundColor: Colors.brown,
              centerTitle: true,
              titleTextStyle: TextStyle(color: Colors.white, fontSize: 20)
              // color: Colors.white
              ),
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        // home: const CountExample(),
        // home: const ExampleOne(),
        home: Favourite(),

      ),
    );
  }
}
