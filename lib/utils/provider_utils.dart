import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/exampleone_provider.dart';
import 'package:provider_overview_01/provider/favourite_provider.dart';

import '../provider/count_provider.dart';

var listOfProvider= [
  ChangeNotifierProvider(
    create: (context) => CountProvider(),
  ),
  ChangeNotifierProvider(
    create: (context) => ExampleOneProvider(),
  ),
  ChangeNotifierProvider(create: (context) => FavouriteProvider(),),
];