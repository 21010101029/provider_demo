import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/exampleone_provider.dart';

class ExampleOne extends StatefulWidget {
  const ExampleOne({super.key});

  @override
  State<ExampleOne> createState() => _ExampleOneState();
}

class _ExampleOneState extends State<ExampleOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Example 1"),
      ),
      body: Consumer<ExampleOneProvider>(
        builder: (context, provider, child) {
          return Center(
            child: Column(
              children: [
                Slider(
                  max: 1,
                  min: 0,
                  onChanged: (val) {
                    provider.setValue(val);
                  }, value: provider.value,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 100,
                        color: Colors.red.withOpacity(provider.value),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 100,
                        color: Colors.green.withOpacity(provider.value),
                      ),
                    ),

                  ],
                ),
              ],
            ),
          );
        },
        // child:
      ),
    );
  }
}
