import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/favourite_provider.dart';

class FavouriteTwo extends StatefulWidget {
  const FavouriteTwo({super.key});

  @override
  State<FavouriteTwo> createState() => _FavouriteTwoState();
}

class _FavouriteTwoState extends State<FavouriteTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FavouriteTwo"),
      ),
      body: Consumer<FavouriteProvider>(
        builder: (context, value, child) {
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        value.addValues(index);
                      },
                      title: Text(index.toString()),
                      trailing: value.list.contains(index)?Icon(Icons.favorite_outlined):Icon(Icons.favorite_outline),
                    );
                  },
                  itemCount: 100,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
