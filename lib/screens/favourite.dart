import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/favourite_provider.dart';
import 'package:provider_overview_01/screens/favourite_demo.dart';

class Favourite extends StatefulWidget {
  const Favourite({super.key});

  @override
  State<Favourite> createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton:
          FloatingActionButton.extended(onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => FavouriteTwo(),));
          }, label: Text("Next"),
          ),
      appBar: AppBar(
        title: Text("Favourite"),
      ),
      body: Consumer<FavouriteProvider>(
        builder: (context, value, child) {
          return Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        value.addValues(index);
                      },
                      title: Text(index.toString()),
                      trailing: value.list.contains(index)
                          ? Icon(Icons.favorite_outlined)
                          : Icon(Icons.favorite_outline),
                    );
                  },
                  itemCount: 100,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
