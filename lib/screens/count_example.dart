import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_overview_01/provider/count_provider.dart';

class CountExample extends StatefulWidget {
  const CountExample({super.key});

  @override
  State<CountExample> createState() => _CountExampleState();
}

class _CountExampleState extends State<CountExample> {
  @override
  Widget build(BuildContext context) {
    
    return Consumer<CountProvider>(
      builder: (context, value, child) {
        return  Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              value.setcount();
            },
            child: const Icon(Icons.add),
          ),
          body: Center(

            child: Text(
              value.count.toString(),
            ),
          ),
          appBar: AppBar(
            title: const Text("Count Example "),
          ),
        );
      },
      // child:
    );
  }
}
